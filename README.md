# Photovoltaics efficiency limits

## This program allows to calculate the photovoltaic efficiency limits of a material based on its absorption coefficient and main recombination parameters.

To use it, simply go into the program directory and download the 
script "Eff_Limit.m", and the two .zip files containing the repositories and 
some useful experimental data to test the program.

Extract the .zip files in the same folder as the script and execute.

*Note that this program was made for Matlab R2017a. While it should run just 
fine in other versions, the script "standard1Dgraph" tends to show problems 
when changing version.

Typical issues:

Absorption coefficient:

1.- Define your absorption coefficient as an energy dependent function that 
does not return NaN values. There is a function in the script called NaNtoZero 
to fix that.

2.- Ensure that the function is defined all the way up to 4.3 eV at least, 
otherwise your Jsc will be underestimated.

3.- The bandedge should be well defined and the Urbach Energy sharp, otherwise 
the Voc will drop dramatically, as thermal energy (kT = 25 meV) cannot out 
compete thermalization, (i.e. Ue > kT). This will give an underestimate of the 
Voc.

***This work lead to the publication: "[The fundamental efficiency limits of lead iodide perovskite solar cells](https://pubs.acs.org/doi/abs/10.1021/acs.jpclett.7b03054)"**