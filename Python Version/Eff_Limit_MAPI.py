######## Eff limit of photovoltaic materials ##########

#### Load Libraries ####
import xlrd
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import scipy.special as special
import functools
import time


########### Control Panel ################

Ctrl = { 
  "Figure1" : "False",
  "Figure2" : "False", 
  "Figure3" : "False" 
        }

#print(Ctrl)

########### Generic Definitions ##########

q = 1.6021e-19	# Electron charge: 	Coulomb
C = 6.2411e18	  # Coulomb:		A s
k = 8.6173e-5	  # Boltzmann constant:	eV K-1
t = 300		      # Temperature:		K
c = 3e8		      # Speed of light:	m s-1
h = 4.13e-15	  # Planck's constant:	J s
kT = k*t	      # Thermal Energy	eV
m = 9e-31	      # Electron mass		Kg

########### Define Generic Functions ###########

def load_excel(str, row, col):
  sheet = xlrd.open_workbook(str).sheet_by_index(0)
  list_of_data  = [[sheet.cell_value(r,c) for c in range(col,sheet.ncols)] for r in range(row, sheet.nrows)]
  array_of_data = np.asarray(list_of_data)
  return array_of_data


########### Load Experimental Data ##################

# Load solar spectrum
Solar_Spectrum = load_excel("Experimental Data/ASTMG173.xlsx", 2, 0)

# Load absorption coefficient data
Abs_Coeff_Data = np.genfromtxt('Experimental Data/Abs. Coeff Corrected.csv', delimiter = ',')

# Define absorption coefficient function
def Abs_Coeff(E):
  return np.interp( E, Abs_Coeff_Data[:,0], Abs_Coeff_Data[:,1] ) 

# This function takes an array of energies and returns the corresponding power
# Array_Energy -> Array Power
def AM15_Pwr(E):
  Wavelength = 1240./E
  return np.interp( Wavelength, Solar_Spectrum[:,0], Solar_Spectrum[:,2] ) * Wavelength**2./1240 

# Array Energy -> Array of Fluxes
def AM15_Flux(E):
  return AM15_Pwr(E)*1.0/E

# Refractive index
nr = 2.6

######### Data for Recombination Rates ##############

# Radiative recombination rate (cm3 s-1)
B = 1.34e-10

# Auger coefficient (cm6 s-1)
C = 1.1e-28


######### Define Absorption Functions ###############

# General Eq. for Textured Device
def Abs_Material_LT(thick, R, E):
  return Abs_Coeff(E) * thick * 1.0/( Abs_Coeff(E)*thick + 1.0/(4*nr**2) + 1.0/4*( 1 - R ) )  

# General Eq. for Flat Device
def Abs_Material_BL(thick, R, E):
  return (1 - np.exp(-Abs_Coeff(E) * thick) ) * ( 1 + R * np.exp(-Abs_Coeff(E) * thick) )

# Perfect mirror Textured Device
def Abs_Material_LT_Ideal_Mrr(thick, E):
  return Abs_Material_LT(thick, 1, E)

# Perfect mirror Flat Device
def Abs_Material_BL_Ideal_Mrr(thick, E):
  return Abs_Material_BL(thick, 1, E)

#### Absorption of internally generated radiation by imperfect mirror ####

def Abs_Internal_Mirror_BL(thick, R, E, theta ):
  abs_belowcriticangle = (1-R) * ( 1 - np.exp(-Abs_Coeff(E) * thick * 1.0 / np.cos(theta) ) )
  abs_abovecriticangle = (1-R) * (1 - np.exp( -2 * Abs_Coeff(E) * thick * 1.0 / np.cos(theta) ) ) / (1 - R * np.exp( -2 * Abs_Coeff(E) * thick * 1.0/ np.cos(theta) ) ) 
  return ( theta <= np.arcsin(1/nr) ) * abs_belowcriticangle + ( theta > np.arcsin(1/nr) ) * abs_abovecriticangle 

                                                  
###### Jsc and JR definition ###### 

# Blackbody radiation flux (Boltzmann Approximation)
def BB_Rad(E,u):
  return 2.0 * E**2 / (h**3*c**2) * np.exp( -(E-u)*1.0/(kT) );  # eV^-1 m-2 s-1  pi comes from the integral over hemisphere

# Photocurrent Limits of Integration

# Photocurrent Function
def Jsc(Abs_fun):
  E_lims = [0.7, 4.4]
  return integrate.quad( lambda E : AM15_Flux(E) * Abs_fun(E), E_lim[0], E_lim[1] )[0]

# External radiative current
def J0( Abs_fun, V ):
  E_lims = [1.2, 2]
  return integrate.quad( lambda E: q * np.pi * Abs_fun(E) * BB_Rad(E,V), E_lims[0], E_lims[1] )[0]

# Internal radiative current
def J0_int( thick, V ):
  return integrate.quad( lambda E : q * 4 * np.pi * thick * nr**2 * Abs_Coeff(E) * BB_Rad(E,V), E_Lims_J0[0], E_Lims_J0[1] )[0] 

# Probability of escape of internally emitted current
def Pesc(Abs_Fun, thick):
  return J0( Abs_Fun, 0 ) * 1.0 / J0_int( thick, 0 )



#################### Calculation of intrinsic carrier density ######################
    
# Internal bimolecular recombination rate
# Using the Van Roosbroeck Relationship we obtain the intrinsic carrier density:

ni = np.sqrt( integrate.quad( lambda E : 4 * np.pi * nr**2 * Abs_Coeff(E) * 1e2 * BB_Rad(E,0) / ( B*1e-6 ), E_Lims_J0[0], E_Lims_J0[1] )[0] ); # m-3

#################### Define Non-Radiative Currents #################################

# Auger current m^-2
def J_auger( C, ni, thick, V ):
  return q * C * thick * (ni * 1e-6)**3 * np.exp( 3.0*V/(2*kT) ) * 1e4

## SRH current m^-2
def J_SRH( A, ni, thick, V):
  return q * A * (ni*1e-6) * thick * np.exp(1.0*V/(2*kT)) * 1e4

# Optical parasitic mirror absorption

# Textured device m^-2
def J0_par_LT( Abs_Fun_Mirror, R, V ):
  return  integrate.quad( lambda E : q * np.pi * Abs_Fun_Mirror( R, E ) * BB_Rad(E,V), E_Lims_J0[0], E_Lims_J0[1] )[0]


# Radiative current hitting mirror
def J0_par_BL( Abs_fun_mirror, R, V  ):
  
  thetaaxis = np.linspace( 0, np.pi/2, num = 5e2); dtheta = thetaaxis[1] - thetaaxis[0];
  Eaxis = np.linspace(0.5, 3.0, num = 1e4);       dEaxis = Eaxis[1] - Eaxis[0];

  rad_flux =  lambda E, theta : q * 2 * np.pi * Abs_fun_mirror( R, E ,theta ) * BB_Rad( E, V ) * nr**2 * np.cos(theta) * np.sin(theta) * dE * dtheta
  
  X, Y = np.meshgrid( Eaxis,thetaaxis )
   
  return np.sum( rad_flux ( X, Y ) )







############     Plot Figures     #####################
#
## Plot absorptivity of material
#Energy_axis = np.linspace(0.5, 3.0, num = 1e4)
#
#plt.plot(Energy_axis, map( lambda E : Abs_Material_BL(1e-4, 1, E), Energy_axis) )
#plt.ylabel('Absorptivity (%)')
#plt.show()
#

# Plot absorptivity of mirror
#Energy_axis = np.linspace(0.5, 3.0, num = 1e4)
#
#plt.plot(Energy_axis, Abs_Internal_Mirror_BL(1e-4, 0.2, Energy_axis, 0))
#plt.ylabel('Absorptivity (%)')
#plt.show()

# Plot BB_Rad
#Energy_axis = np.linspace(0, 3.0, num = 1e4)
#
#plt.plot(Energy_axis, BB_Rad( Energy_axis, 1 ))
#plt.ylabel('BB_Radiation')
#plt.show()
