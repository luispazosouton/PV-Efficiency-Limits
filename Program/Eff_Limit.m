% This program was originally written by Luis M. Pazos Outon in December 2017.
%
% If the use of this program leads to published work, please cite our original manuscript: 
% Pazos-Outon et al. "Fundamental efficiency limit of MAPbI3 solar cells", The journal of Physical Chemistry Letters, 2018.

%% Start Program:

clear all; close all;

%% Control panel - This section will define which sections of the script are executed.

% Folder to save images
image_folder = 'Figures';

% This defines whether the device is 'Flat' or 'Textured'
Ctrl.Surface = 'Flat';

% Define which figures should be executed
Ctrl.Figure1         = true;  % Figure 1. - JV Parameters and PLQE vs thickness
Ctrl.Figure2         = true;  % Figure 2. - JV Parameters and PLQE vs Reflectivity
Ctrl.Figure3         = true;  % Figure 3. - JV Parameters and PLQE vs SRH

% Define which calculations should be done
Calculation.Figure1  = true;
Calculation.Figure2  = true;
Calculation.Figure3  = true;

disp(Ctrl)

%% Load libraries

LibLoader = @(RootFolder,LibNames) cellfun( @addpath, ( arrayfun( @(str) strcat(RootFolder,str), LibNames ))); % This function loads libraries nested in a root folder

% Load repositories
RepFolder = 'Repositories/';
LibLoader( RepFolder,{'Functional Functions','Figure Treatment','Raw Data Treatment','Directory','Search', 'Signal Analysis', 'MAPI_Eff_Lim_subfunctions'} )

%% Generic Definitions of constants and functions

% Define Constans

q = 1.60217e-19;    % Electron charge in Units:     Coulomb
C = 6.2411e18;      % Coulomb in Units:             A s
k = 8.6173e-5;       % Boltzmann constant in Units:  m2 kg s-2 K-1

t = 300;            % Temperature in Units:         K
c = 3e8;            % Speed of light in Units:      m s-1
h = 4.13e-15;       % Planck's constant in Units:   eV s 
h_SI = 6.626e-34;   % Planck's constant in Units:   J s

kT = k*t;           % Thermal Energy                eV
m = 9e-31;          % Electron mass                 Kg

%%% Define Expressions:

% Removes Nan values from data
 rmNaN = @(vec) vec(~isnan(vec));

% Define specific function of nth_output
 sec_out = @(func,vec) nth_output(2,func,vec);

% Index an array of n-dimensions
 arrayind = @(vec,num) vec(num); 

% Cell to array
 celltoarray = @(cell) [ cell{:} ];

% Find x from y
 find_x_from_y = @(xvec, yvec, yval) arrayind( xvec, sec_out( @min, abs( yvec - yval ) ) );

% Optimize Graph limits for esthetic purposes
 opt_lims = @( Lims_raw ) [ Lims_raw(1) - ( Lims_raw(2) - Lims_raw(1) )/16, Lims_raw(2) + ( Lims_raw(2) - Lims_raw(1) )/16 ];

%% Load Experimental Data

%%%%%%%%% Load solar standard spectrum
ASTMG173_data        =   xlsread( 'Experimental Data/ASTMG173.xlsx','','','basic' );

%%% Incoming Radiation

AM15.lambda_Power =     @(lambda)   interp1(ASTMG173_data(:,1)',ASTMG173_data(:,3)',lambda);  % Power density (W m^-2 lambda^-1) vs Wavelength
AM15.Power =            @(E)        1240./(E.^(2)) .* AM15.lambda_Power(1240./E);             % Power density (W m^-2 eV^-1) vs Energy (Jacobian corrected)

%Calculate flux in A/m^2 (Note C = Coulomb)
AM15.Flux = @(E) AM15.Power(E)./E;

% Total incoming power (W/m^2)
Incoming_power = sum(map([280:4000],AM15.lambda_Power));

 
%%%%%%%% Load absorption coefficient ( in cm-1 )  %%%%%%%%%
Absorption_data      =   dlmread( 'Experimental Data/Abs. Coeff Corrected.csv' );

% Functionalize absorption coefficient
Abs_coeff = @(E) interp1( Absorption_data(:,1), Absorption_data(:,2), E );

% Load refractive index spectrum, otherwise plug number and modify equations. 
nr = @(E) NaNtoZero( real( nperov(1240./E) ) ) .* (E <= 4) + real( nperov(1240./4) ).* (E > 4) ;       % Refractive index

% Approximate refractive index at edge
nr_edge = 2.6;

%%% Data for recombination rates (Richter et al.):

% Radiative recomb. rate
b_int = 1.34e-10;  % cm3 s-1

% Auger coefficient
c_Auger = 1.1e-28;   % cm^6 s-1

%% Define Absorption functions

%%% Absorption in the material with an imperfect mirror %%%

 % Textured device
  Abs_Material_Mirror_LT =  @(t, R, E) Abs_coeff(E).*t ./ ( Abs_coeff(E).*t + 1./(4.*nr(E).^2) + 1/4*(1-R) );

 % Flat device
  Abs_Material_Mirror_BL =  @(t, R, E ) ( 1 - exp(-Abs_coeff(E).*t) ) .* ( 1 + R * exp(-Abs_coeff(E).*t) );

%%% Absorption in the material with a perfect mirror %%%

 % Absorptivity Light Trapping
  Abs_LT = @( t, E ) Abs_Material_Mirror_LT( t, 1, E );
  
 % Absorptivity Beer-Lambert
  Abs_BL = @( t, E ) Abs_Material_Mirror_BL( t, 1, E);

%%% Absorption of internally generated radiation by the imperfect mirror %%%

%%%%%%%%% General definition of mirror absorptivity %%%%%%%%% 
Abs_Internal_Mirror = @(t, R, E, theta )  (1-R) .* (...
                                                ( ( 1 - exp(-Abs_coeff(E) .* t./ cos(theta) ) )  .* ( theta <= asin(1/nr_edge) ) ) + ... 
                                                (1 - exp( -2 .* Abs_coeff(E) .* t./ cos(theta) ) ) ./ (1 - R .* exp( -2 .* Abs_coeff(E) .* t./ cos(theta) ) ) .* ( theta > asin(1/nr_edge) ) ...
                                                                  )  ;
  
%% Jsc and JR definition 

% Blackbody radiation flux (Boltzmann Approximation)
BB_rad = @(E,u) 2 * E.^2 ./ (h^3*c^2) .* exp( -(E-u)/(kT) );  % eV^-1 m-2 s-1  pi comes from the integral over hemisphere

% Photocurrent
E_Lims_Jsc = [0.7 4.4];
Jsc = @(Abs_fun) integral( @(E) AM15.Flux(E) .* Abs_fun(E), E_Lims_Jsc(1), E_Lims_Jsc(end) );

% External radiative current
E_Lims_J0 = [1.2 2];
J0 = @( Abs_fun, V ) integral( @(E) q * pi * Abs_fun(E) .* BB_rad(E,V'), E_Lims_J0(1), E_Lims_J0(end) );

% Internal radiative current
J0_int = @( thickness, V ) integral( @(E) q * 4 * pi * thickness * nr(E).^2 .* Abs_coeff(E) .* BB_rad(E,V), E_Lims_J0(1), E_Lims_J0(end) ); 

% Probability of escape of internally emitted current
Pesc = @(Abs_fun, thickness) J0( Abs_fun, 0 ) ./ J0_int( thickness, 0 );

%% Calculation of intrinsic carrier density
    
%%% Internal bimolecular recombination rate
% Using the Van Roosbroeck Relationship we obtain the intrinsic carrier density:
% ni^2 = sum ( 4 * pi * nr.^2 * Abs_coeff * 2  * E.^2./(h^3*c^2).*exp(-q*E/(k*t))) )  * dE3  / bint

ni = sqrt( integral( @(E) 4 * pi * nr(E).^2 .* Abs_coeff(E) *1e2 .* BB_rad(E,0) / ( b_int*1e-6 ), E_Lims_J0(1), E_Lims_J0(end) ) ); % m-3

% Action law
n = @(V) ni*1e-6*exp(V/(2*kT));

%% Define Non-Radiative Currents

% Auger current m^-2
J_auger = @( C_Auger, ni, thickness, V ) q * C_Auger * thickness * (ni * 1e-6)^3 * exp(3*V/(2*kT)) * 1e4;

% SRH current m^-2
J_SRH   = @( A_SRH, ni, thickness, V) q * A_SRH * (ni*1e-6) * thickness * exp(V/(2*kT)) * 1e4;

% Optical parasitic mirror absorption

% Textured device m^-2
J0_par_LT = @( Abs_fun_mirror, R, V )  integral( @(E) q * pi * Abs_fun_mirror( R, E ) .* BB_rad(E,V), E_Lims_J0(1), E_Lims_J0(end) );

% Flat device m^-2

% Radiative current hitting mirror
% Define integration axis
E_axis = [1.2:1e-4:2];
theta_axis = 0:pi/2/100:pi/2;

% Define delta
dE = E_axis(2) - E_axis(1);
dtheta = theta_axis(2)-theta_axis(1);
 
% Define integral boundaries
[ X , Y ] = meshgrid( E_axis , theta_axis );

% Define function    
J0_par_BL = @( Abs_fun_mirror, R, V ) sum( sum( feval( @( E, theta ) q * 2 * pi * Abs_fun_mirror( R, E ,theta ) .* BB_rad( E, V ) .* nr( E ).^2 * dE * dtheta , X , Y ) ) );

%% Define PLQE functions

% External PLQE function
PLQE  = @( J1, J2_rad, J2_non_rad, J3, V) J2_rad .* exp( V/kT ) ./ ( J1 .* exp( 1/2 * V/kT ) + ( J2_rad + J2_non_rad ) .* exp( V/kT ) + J3 .* exp( 3/2 * V/kT ) );
    


%% Generate Figures

%% Figure 1. - JV Parameters and PLQE vs thickness

if Ctrl.Figure1
    
Folder = 'Figure 1';


% Define thickness axis
thick_ax = [0.001:0.001:0.019 0.02:0.01:1]*1e-4;
    

%%% Calculate values
if Calculation.Figure1
    
    
clear JV_Aug_BL JV_rad_BL JV_Aug_LT JV_rad_LT
tic
for i = 1:length(thick_ax)

    counter = length(thick_ax) - i;

    % Define absorption function
    if strcmp( Ctrl.Surface, 'Textured' )

     Abs_fun = @(E) Abs_LT( thick_ax(i), E );
    
    elseif strcmp( Ctrl.Surface, 'Flat' )
       
     Abs_fun = @(E) Abs_BL( thick_ax(i), E );
        
    end
    
    % Define currents
    Jsc_val = Jsc(Abs_fun) * 0.1;
    J1 = 0 * 0.1;
    J2 = J0( Abs_fun, 0 ) * 0.1;
    J2_int = J0_int( thick_ax(i), 0 ) * 0.1;
    J3 = J_auger( c_Auger, ni, thick_ax(i), 0 ) * 0.1;
    
    
    % Send handles of functions to JV_Analysis
    JV_Aug_LT(i,:) = JV_Analysis_Opt( Jsc_val, 0, J2, J3 );
    JV_rad_LT(i,:) = JV_Analysis_Opt( Jsc_val, 0, J2, 0  ) ;


    % Name Voc and Vop to facilitate code readability
    Voc_thick = JV_Aug_LT(i,2);
    Vop_thick = JV_Aug_LT(i,5);
    
    % PLQE calculation
    
    % At open-circuit
    PLQE_Voc(i)     = PLQE( 0, J2,     0, J3, Voc_thick );
    PLQE_int_Voc(i) = PLQE( 0, J2_int, 0, J3, Voc_thick );

    % At operating-point
    PLQE_Vop(i)     = PLQE( 0, J2,     0, J3, Vop_thick );
    PLQE_int_Vop(i) = PLQE( 0, J2_int, 0, J3, Vop_thick );

    % Estimate escape probability
    Prob_esc(i) = Pesc( Abs_fun, thick_ax(i) );
    
    % Carrier density
    n_Aug(i) = ni*exp( JV_Aug_LT(i,2)/(2*kT) )*1e-6;
    n_rad(i) = ni*exp( JV_rad_LT(i,2)/(2*kT) )*1e-6;
    
    
    time = toc;
    timeleft = time * counter / ( length(thick_ax) - counter );

    disp( ['Time left: ' num2str(timeleft/60,3) ' minutes'] ) 
    
end


    % Save generated data
    makedir([image_folder '/' Folder])
    save([image_folder '/' Folder '/Y_vals.mat'], 'JV_Aug_LT', 'JV_rad_LT','PLQE_Voc', 'PLQE_int_Voc', ...
         'PLQE_Vop', 'PLQE_int_Vop', 'Prob_esc', 'n_Aug', 'n_rad', 'thick_ax')

else 
    % Load generated data
    load([image_folder '/' Folder '/Y_vals.mat'], 'JV_Aug_LT', 'JV_rad_LT','PLQE_Voc', 'PLQE_int_Voc', ...
         'PLQE_Vop', 'PLQE_int_Vop', 'Prob_esc', 'n_Aug', 'n_rad', 'thick_ax' )

end 
    
%%% Plot JV Parameters



%%% Define plot conditions

XLimit = [ 0 1 ];
Labels =    { 'Jsc (mA cm^-^2)', 'V_{oc} (V)', 'Fill Factor (%%)', 'Power Conversion\n Efficiency (%%)',...
              'V_{op} (V)', 'External PLQE (%%)', 'Internal PLQE (%%)', 'Prob. Escape (%%)', 'Carr. Conc. (10^{-16} cm^{-3})'  };
          
Filenames = { 'Jsc (mA cm^-^2)', 'Voc (V)', 'Fill Factor (%)', 'Power Conversion Efficiency (%)', 'Vop (V)',...
              'Ext. PLQE', 'Int. PLQE', 'Prob. Escape', 'Carr. Conc' };

% Define data to plot
YData1 = { JV_Aug_LT(:,1), JV_Aug_LT(:,2), JV_Aug_LT(:,3) * 100, JV_Aug_LT(:,4), JV_Aug_LT(:,5),...
            PLQE_Voc(:)*100 , PLQE_int_Voc(:)*100, Prob_esc(:)*100, n_Aug(:)*1e-16 };

YData2 = { JV_rad_LT(:,1), JV_rad_LT(:,2), JV_rad_LT(:,3) * 100, JV_rad_LT(:,4), JV_rad_LT(:,5),...
           100*ones(1,length(thick_ax)), 100*ones(1,length(thick_ax)), Prob_esc(:)*100, n_rad(:)*1e-16 };

% Y axis limits
yaxis_raw = {   [23 27], [1.25 1.31],      [90.1 90.4],     [27 31], [1.16 1.21], [94 100], [98 100], [0 100], [0 5.5] };
  
% Y Ticks
YTicks = {  [23:30], [1.2:0.01:1.35], [88:0.1:92], [24:32], [1.1:0.01:1.25], [0:1:100], [0:0.2:100], [0:10:100], [0:1:5] };
YTicksLabels = ...
    { {num2str(YTicks{1}')}, {num2str(YTicks{2}')}, {num2str(YTicks{3}')}, {num2str(YTicks{4}')}, {num2str(YTicks{5}')}, ...
      {num2str(YTicks{6}')}, {num2str(YTicks{7}')}, {num2str(YTicks{8}')}, {num2str(YTicks{9}')} };
  
% X Ticks
XTicks = [0:0.2:2];
XMinorTicks = [0:0.1:2];
   
% Ring density determines the amount of red circles
ring_density = 3;


% Run loop to plot all JV Parameters

for i = 1:length(Labels)
    
    figure('Visible', 'off')

    % Create plot
    qu(1) = plot( thick_ax*1e4, YData1{i} , 'LineWidth', 5, 'Color', 'k' );
    hold on
    qu(2) = plot( thick_ax*1e4, YData2{i}, 'LineStyle','none', 'Color', 'r', 'Marker', 'o', 'MarkerSize', 22, 'MarkerFaceColor', 'r' );
    qu(3) = plot( thick_ax*1e4, YData2{i}, 'LineStyle','-', 'LineWidth', 5, 'Color', 'r' );

    hold off
    
    % Set Auger limit on top    
    uistack(qu(1),'top')

    % Arrange density of red rings
    xdata = get(qu(2),'XData');
    ydata = get(qu(2),'YData');
    set(qu(2),'XData',xdata( [ 1:10*ring_density:20, 21:ring_density:length(xdata) ]) );
    set(qu(2),'YData',ydata( [ 1:10*ring_density:20, 21:ring_density:length(xdata) ]) );

    % Fix Axis limits and tick positions
    set( gca, 'YLim', opt_lims( yaxis_raw{i} ), 'YTick',YTicks{i},'YTickLabel', YTicksLabels{i}, 'XLim', XLimit )
    set( gca, 'XTick', XTicks )
    
    % Set labels
    ylabel(sprintf(Labels{i})); xlabel('Thickness (\mum)'); 
    
    % Arranges default font types and sizes
    standard1Dgraph
    
    % Correct Minor Ticks
    set( gca, 'XMinorTick', 'on' )
    ax = gca;
    ax.XAxis.MinorTickValues = XMinorTicks;

   
    % Save Figure
    makedir([image_folder '/' Folder ])
    saveas(gcf,[image_folder '/' Folder '/' Filenames{i} ' vs Thickness.png']);
    saveas(gcf,[image_folder '/' Folder '/' Filenames{i} ' vs Thickness.fig'])

    
end

end

%% Figure 2. - JV Parameters and PLQE vs Reflectivity

if Ctrl.Figure2
    
Start = 'Start Figure 2';
Folder = 'Figure 2';

% Device parameters
thick = 0.5e-4;

% Define absorptivity for imperfect mirror
    if strcmp( Ctrl.Surface, 'Textured' )

     % Absorptivity of material
     Absorptivity =  Abs_Material_Mirror_LT;
     
     % Absorptivity of mirror to internally generated light
     Abs_Mirror   =  @( R, E ) Abs_Material_Mirror_LT ( thick, R, E ) .* nr(E).^2 .* ( 1 - R );
     
     % Define non-radiative optical parasitic absorption current
     J0_non_rad   =  J0_par_LT;
    
    elseif strcmp( Ctrl.Surface, 'Flat' )
     
     % Absorptivity of material
     Absorptivity =  Abs_Material_Mirror_BL;
 
     % Absorptivity of mirror to internally generated light
     Abs_Mirror   =  @( R, E, theta ) Abs_Internal_Mirror ( thick, R, E, theta );

     % Define non-radiative optical parasitic absorption current
     J0_non_rad   =  J0_par_BL;

    end


% Define reflectivity axis
R_axis = [0.9:0.001:1];

% Generate data
if Calculation.Figure2

tic

        
for i = 1:length(R_axis)
    
    % Start counter
    counter = length(R_axis) - i;
    
    % Define absorption function
    Absorptivity_R = @(E) Absorptivity (thick, R_axis(i), E );
   
    % Parameters MAPbI3
    Jsc_val    = Jsc        ( Absorptivity_R ) * 0.1;
    J1         = 0;
    J2_rad     = J0         ( Absorptivity_R, 0 ) * 0.1;
    J2_non_rad = J0_non_rad ( Abs_Mirror, R_axis(i), 0 ) * 0.1;
    J2_int     = J0_int     ( thick, 0 ) * 0.1;
    J3         = J_auger    ( c_Auger, ni, thick, 0 ) * 0.1;
    
    % Extract parameters  
    JV_Total_Mirror_LT(i,:) = JV_Analysis_Opt( Jsc_val, J1, J2_rad + J2_non_rad, J3 );

    % Label open-circuit voltages
    V_oc = JV_Total_Mirror_LT(i,2);
   
    % Radiative voltages with modified absorptivity function due to mirror
    Voc_rad(i) = kT*log( Jsc_val / J2_rad );

    %Extract PLQE MAPbI3  
    PLQE_R(i) = PLQE( 0,J2_rad, J2_non_rad, J3, V_oc);
     
    % Internal PLQE MAPbI3
    PLQE_int_R(i) = PLQE( 0, J2_int, 0, J3, V_oc);
    
    % Probability of escape
    Pesc_mirror(i) = J2_rad / J2_int;
    
    % Carrier concentration
    carr_conc(i) = n(V_oc);
    
    time = toc;
    timeleft = time * counter / ( length(R_axis) - counter );

    disp( ['Time left: ' num2str(timeleft/60,3) ' minutes'] ) 

end
    makedir( [image_folder '/' Folder] )
    save( [image_folder '/' Folder '/Y_vals.mat'] )

else
    
    load([image_folder '/' Folder '/Y_vals.mat'], 'JV_Total_Mirror_LT', 'PLQE_R', 'PLQE_int_R', 'carr_conc', 'Pesc_mirror')

end

% Define data to be plotted
Y_Data = {JV_Total_Mirror_LT(:,1)', JV_Total_Mirror_LT(:,4)', JV_Total_Mirror_LT(:,2)', PLQE_R * 100,...
          JV_Total_Mirror_LT(:,3)' * 100, JV_Total_Mirror_LT(:,5)', PLQE_int_R * 100, 1e-16 * carr_conc, 100 * Pesc_mirror};

% Y axis labels
Y_label = {'Jsc (mA cm^-^2)', 'Power Conversion\nEfficiency (%%)', 'Voc (V)', 'External Luminescence\n efficiency (%%)',...
           'Fill Factor (%%)', 'Vop (V)', 'Internal PLQE (%%)', 'Carrier Conc (10^1^6 cm^-^3)', 'Probability of escape (%%)'};
       
% Y axis tick positions       
Y_Ticks = { [20:0.5:28], [26:0.5:32], [1.21:0.01:1.30], [0:20:100], [0.8:0.0005:1]*100,...
            [1.1:0.01:1.2], [88:0.2:100], [0.1:0.1:1.3], [0:5:20] };
        
% Y axis limits        
Y_Lims_raw  = { [26 27], [29.5 30.5], [1.26 1.28], [0 100], [0.9 0.903]*100, [1.16 1.18], [99 100],...
                [0.7 1.2], [0 20] };

% Set filenames
Filenames = {'Jsc (mA cm^-^2)', 'Efficiency (%)', 'Voc (V)', 'External PLQE (%)', 'Fill Factor (%)',...
             'Vop (V)', 'Internal PLQE (%)', 'Carrier Conc (10^1^6 cm^-^3)', 'Probability of escape' };

% X axis tick positions
XTicks = [90:2:100]; XMinorTicks = [90:1:100];

% Create folder to save images
makedir([image_folder '/' Folder ])

% Make plots

for i = [1:length(Y_Data)]

    figure('Visible','off')
    plot( R_axis*100, Y_Data{i},'LineWidth', 5 , 'Color', 'k')
   
    % Define axis labels
    xlabel('Mirror Reflectivity (%)')
    ylabel(sprintf(Y_label{i}))
    
    % Set Axis limits and tick positions
    set(gca, 'YLim', opt_lims( Y_Lims_raw{i} ), 'YTick',Y_Ticks{i} )
    set(gca, 'XLim',[90 100], 'XTick', XTicks)
    
    % Set default font types and sizes
    standard1Dgraph

    % Refine X-ticks
    ax = gca;
    ax.XAxis.MinorTick = 'on';
    ax.XAxis.MinorTickValues = XMinorTicks;
           
    % Save figure
    saveas(gcf,[image_folder '/' Folder '/' Filenames{i} ' vs Reflectivity.png'])
    saveas(gcf,[image_folder '/' Folder '/' Filenames{i} ' vs Reflectivity.fig'])

end


end

%% Figure 3. - JV Parameters and PLQE vs SRH

if Ctrl.Figure3
    
 disp('Start Figure 3');   
 Folder = 'Figure 3';

  
 if Calculation.Figure3
        
    % Device parameters
     thick = 0.5e-4;
     
    % Define absorptivity
    if strcmp( Ctrl.Surface, 'Textured' )

    % Absorptivity of material
     Absorptivity =  @(E) Abs_LT(thick, E);
         
    elseif strcmp( Ctrl.Surface, 'Flat' )

     
    % Absorptivity of material
     Absorptivity =  @(E) Abs_BL(thick, E);
 
    end

     
   % Calculate JV Curves

   % Range of SRH A constants
    a_axis = [0 10.^[3:0.1:5] 10.^[5:0.1:7]];
   
   % Define Current coefficients
    Jsc_val = Jsc( Absorptivity )              * 0.1;
    J2 =      J0( Absorptivity, 0 )            * 0.1;
    J2_int =  J0_int( thick, 0 )               * 0.1;
    J3 =      J_auger( c_Auger, ni, thick, 0 ) * 0.1;  
   
   for i = 1:length(a_axis)
       
       counter = length(a_axis) - i;
       
       disp([ 'Remaining Cycles: ' num2str(counter) ])
       
       
       % Calculate change in SRH coefficient
        J1 = J_SRH( a_axis(i), ni, thick, 0 ) * 0.1;

       % Calculate Curves
        JV_Parameters(i,:) = JV_Analysis_Opt( Jsc_val, J1, J2, J3 );

       % Re-label voltage
        V_oc = JV_Parameters(i,2);

       % External Luminescence
         PLQEs_ext(i) = PLQE( J1, J2, 0, J3, V_oc );
         
       % Internal Luminescence
         PLQEs_int(i) = PLQE( J1, J2_int, 0, J3, V_oc );
         
   end
   
   % Rename quantities for clarity
   Jscs  = JV_Parameters( :, 1 );
   Vocs  = JV_Parameters( :, 2 );
   FFs   = JV_Parameters( :, 3 );
   Effs  = JV_Parameters( :, 4 );
   Vops  = JV_Parameters( :, 5 );

   % Save generated data
   makedir( [image_folder '/' Folder] )
   save( [image_folder '/' Folder '/Y_vals.mat'], 'PLQEs_ext', 'PLQEs_int', 'Jscs', 'Vocs', 'FFs', 'Effs', 'Vops', 'a_axis' )
   
 else
    
   load( [image_folder '/' Folder '/Y_vals.mat'], 'PLQEs_ext', 'PLQEs_int', 'Jscs', 'Vocs', 'FFs', 'Effs', 'Vops', 'a_axis' )
   
 end
 

 
 %%%%%%%%%%%%%%%% Plot figures  %%%%%%%%%%%%%%%%%%%
 
 Folder = 'Figure 3';

 % Filenames for figures        
  Filenames = { 'Voc','Vop','Power Conversion Efficiency', 'Fill Factor', 'Ext. Luminescence', 'Int. Luminescence'};

 % Y-axis data
  YData = { Vocs, Vops, Effs, FFs*100, PLQEs_ext*100, PLQEs_int*100 };
 
 % Y-axis Labels
  YLabels = { 'Voc (V)', 'Vop (V)' sprintf('Power conversion\n efficiency (%%)\n'),sprintf('Fill Factor (%%)\n'),...
             [sprintf('External luminescence\n efficiency') ' (\eta_{ext}%)'], [sprintf('Internal luminescence\n efficiency') ' (\eta_{ext}%)'] };
 
 % Define Y-axis limits
  yaxis = { opt_lims([1 1.3]), opt_lims([0.9 1.2]), opt_lims([23 31]), opt_lims([80 90]), [0 100], [0 100] };
  
 % Define Y-axis tick positions
  YTicks = { [1:0.05:1.5], [0.8:0.05:1.3], [21:1:35], [80:2:91], [0:20:100], [0:20:100] };

 % X-axis data
  Lifetime = 1./a_axis;
 
 % X-axis tick positions
  XTicks = 10.^[-7:1:3];

 % Start loop to generate plots
 for i = 1:length(YData)
 
    semilogx(Lifetime, YData{i}, 'LineWidth', 3,'Color', 'k')
 
    set(gcf,'Visible', 'off')
    
    % Set tick positions
    set(gca, 'XTick',XTicks, 'YTick',YTicks{i})
    
    % Set axis limits
    set(gca, 'XLim', [1e-7 1e-3], 'YLim', yaxis{i} )

    % Label axis
    xlabel('SRH Lifetime (\tau_{SRH} s)'); ylabel(YLabels{i});
 
    % Default parameters
    standard1Dgraph
    grid on
 
    % Adjust further details
    set(gca, 'FontSize'   ,  40);
    set([get(gca,'XLabel'), get(gca,'YLabel')], ...
    'FontSize'   , 55 );
    set( gca, 'XMinorGrid' , 'on');
 
    % Save files
    makedir([image_folder '/' Folder])
    saveas(gcf,[image_folder '/' Folder '/' Filenames{i} ' vs SRH Lifetime' '.png'])

 end
 
end


